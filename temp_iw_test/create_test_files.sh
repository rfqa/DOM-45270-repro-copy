# this should create a 1GB file faster but with null characters
#dd if=/dev/zero of=1GB_file.txt count=1024 bs=1048576 
 
# create a 3GB file with null characters
dd if=/dev/zero of=3GB_file.txt count=3072 bs=1048576 
#create a 1GB file
dd if=/dev/zero of=1GB_file.txt count=1024 bs=1048576 
#df -h /mnt/code

 
# adjusting count will determine size, count=100 is 100Mb in approx 10 sec
#dd if=/dev/urandom of=test_file178.txt bs=1048576 count=178
df -h /mnt/code
 
dd if=/dev/urandom of=test_file300.txt bs=1048576 count=300
#df -h /mnt/code

# adjusting count will determine size, count=100 is 100Mb in approx 10 sec
#dd if=/dev/urandom of=test_file201.txt bs=1048576 count=201
df -h /mnt/code
echo done